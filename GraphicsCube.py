from PIL import Image
from cube import Rubix
#TODO Refactor all the shitty code
#TODO Add comments

colours = {0:"colour/Blue.png", 1:"colour/Green.png", 2:"colour/Orange.png", 3:"colour/Red.png",
           4:"colour/White.png", 5:"colour/Yellow.png"}

def createImage(face, name):
	face = flatten(face)
	name += ".png"
	imagesToProcess = []
	images = []
	for sector in face:
		imagesToProcess.append(colours[sector])
	for i in imagesToProcess:
		images.append(Image.open(i))

	widths, heights = zip(*(i.size for i in images))

	totalWidth = max(widths)*3
	totalHeight = max(heights)*3

	newImage = Image.new("RGB", (totalWidth, totalHeight))

	xOffset = 0
	yOffset = 0
	iteration = 0
	for y in range(3):
		for x in range(3):
			newImage.paste(images[iteration], (xOffset, yOffset))
			xOffset += images[iteration].size[0]
			iteration += 1
		xOffset = 0
		yOffset += images[x+y].size[1]


	newImage.save(name)

def flatten(iterables):
    return (elem for iterable in iterables for elem in iterable)

def superflip(cube):
	cube.move("R'")
	cube.move("U2")
	cube.move("B")
	cube.move("L'")
	cube.move("F")
	cube.move("U'")
	cube.move("U")
	cube.move("B")
	cube.move("D")
	cube.move("F")
	cube.move("U")
	cube.move("D'")
	cube.move("L")
	cube.move("D2")
	cube.move("F'")
	cube.move("R")
	cube.move("B'")
	cube.move("D")
	cube.move("F'")
	cube.move("U'")
	cube.move("B'")
	cube.move("U")
	cube.move("D'")

def create3DCube():
	#names = ["Front.png", "Back.png", "Left.png", "Right.png", "Up.png", "Down.png"]
	relativeFaces = {"Front":["Right.png", "Front.png", "Up.png"], "Back":["Left.png", "Back.png", "Up.png"], "Left":[""]}

	#(a x + b y + c, d x + e y + f)
	shear_vert = (1, 0, -90, 0.5, 1, -128)
	shear_horizon = (1, 2, -180, 0, 2, -90)

	right = Image.open("Right.png")
	front = Image.open("Front.png")
	top = Image.open("Up.png")

	top_image = top.transform((200,200), Image.AFFINE, shear_horizon)
	data = top_image.getdata()
	alpha_top_image = top_image.convert("RGBA")
	newData = []

	for pixel in data:
		if pixel[0] == 0 and pixel[1] == 0 and pixel[2] == 0:
			newData.append((0,0,0,0))
		else:
			newData.append(pixel)
	alpha_top_image.putdata(newData)
	alpha_top_image.save("ts.png")
	right_image = right.transform((200, 200), Image.AFFINE, shear_vert)
	right_image.paste(alpha_top_image, (0,0), alpha_top_image)
	right_image.paste(front,(15,84))
	right_image.save("rs.png")

if __name__ == "__main__":
	cube = Rubix()
	for face in cube.faces:
		createImage(flatten(cube.getFace(face)), face)
	create3DCube()