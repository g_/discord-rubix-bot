from GraphicsCube import createImage, create3DCube, superflip
from cube import Rubix
import settings
from discord.ext.commands import Bot

cube = Rubix()

rubix = Bot(command_prefix="!")

@rubix.command(pass_context = True)
async def show(ctx):

	for face in cube.faces:
		createImage(cube.getFace(face), face)
	create3DCube()
	with open("rs.png", "rb") as file:
		await rubix.send_file(ctx.message.channel, file)

@rubix.command(pass_context = True)
async def up(ctx):

	cube.move("U")
	for face in cube.faces:
		createImage(cube.getFace(face), face)
	create3DCube()
	with open("rs.png", "rb") as file:
		await rubix.send_file(ctx.message.channel, file)

@rubix.command(pass_context = True)
async def down(ctx):

	cube.move("D")
	for face in cube.faces:
		createImage(cube.getFace(face), face)
	create3DCube()
	with open("rs.png", "rb") as file:
		await rubix.send_file(ctx.message.channel, file)

@rubix.command(pass_context = True)
async def left(ctx):

	cube.move("L")
	for face in cube.faces:
		createImage(cube.getFace(face), face)
	create3DCube()
	with open("rs.png", "rb") as file:
		await rubix.send_file(ctx.message.channel, file)

@rubix.command(pass_context = True)
async def right(ctx):

	cube.move("R")
	for face in cube.faces:
		createImage(cube.getFace(face), face)
	create3DCube()
	with open("rs.png", "rb") as file:
		await rubix.send_file(ctx.message.channel, file)

@rubix.command(pass_context = True)
async def front(ctx):

	cube.move("F")
	for face in cube.faces:
		createImage(cube.getFace(face), face)
	create3DCube()
	with open("rs.png", "rb") as file:
		await rubix.send_file(ctx.message.channel, file)

@rubix.command(pass_context = True)
async def back(ctx):

	cube.move("B")
	for face in cube.faces:
		createImage(cube.getFace(face), face)
	create3DCube()
	with open("rs.png", "rb") as file:
		await rubix.send_file(ctx.message.channel, file)

@rubix.command(pass_context = True)
async def super(ctx):
	superflip(cube)
	for face in cube.faces:
		createImage(cube.getFace(face), face)
	create3DCube()
	with open("rs.png", "rb") as file:
		await rubix.send_file(ctx.message.channel, file)
	with open("Back.png", "rb") as file:
		await rubix.send_file(ctx.message.channel, file)
	with open("Down.png", "rb") as file:
		await rubix.send_file(ctx.message.channel, file)
	with open("Left.png", "rb") as file:
		await rubix.send_file(ctx.message.channel, file)

if __name__ == "__main__":
	print("https://discordapp.com/api/oauth2/authorize?client_id={}&scope=bot&permissions=0".format(settings.DISCORD_CLIENT))
	rubix.run(settings.DISCORD_BOT_TOKEN)
	for face in cube.faces:
		createImage(cube.getFace(face), face)


