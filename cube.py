class Rubix():

	#TODO: Something to do with moves is incorrect, need to investiage

	cube = {}
	faces = ["Front", "Back", "Left", "Right", "Up", "Down"]


	def __init__(self, dimension = 3):
		#Faces of a rubix cube = 9*6 = 54
		for x in range(6):
			face = [[x, x, x], [x, x, x], [x, x, x]]
			self.cube.update({self.faces[x]:face})

	def getFace(self, face):
		#Doesn't need to be here, but its better than interacting with the cube itself
		return self.cube[face]

	def setFace(self, face, newFace):
		self.cube.update({face:newFace})

	def getRow(self, row, face):
		if (row == "Top"):
			return self.getFace(face)[0]
		elif (row == "Bottom"):
			return self.getFace(face)[2]
		elif (row == "Left"):
			cubeFace = self.getFace(face)
			returnList = [cubeFace[0][0], cubeFace[1][0], cubeFace[2][0]]
			return returnList
		elif (row == "Right"):
			cubeFace = self.getFace(face)
			returnList = [cubeFace[0][2], cubeFace[1][2], cubeFace[2][2]]
			return returnList

	def setSector(self, face, row, column, data):
		self.cube[face][row][column] = data

	def setRow(self, row, face, array):
		#Do more validation
		if (row == "Top"):
			self.cube[face][0] = array
		elif (row == "Bottom"):
			self.cube[face][2] = array
		elif (row == "Left"):
			for x in range(3):
				self.setSector(face, x, 0, array[x])
		elif (row == "Right"):
			for x in range(3):
				self.setSector(face, x, 2, array[x])

	def move(self, move):
		if len(move) == 1:
			turns = 1
		else:
			try:
				turns = int(move[-1:])
			except ValueError:
				#Not an integer (probs a ')
				#' is counter clockwise, i.e. 3 turns
				turns = 3
		moveType = move[:1]
		for x in range(turns):
			if moveType == "F":
				frontFace = self.cube["Front"]
				self.setFace("Front", [list(ls) for ls in  zip(*frontFace[::-1])])
				bottomUpRow = self.getRow("Bottom", "Up")
				leftRightRow = self.getRow("Left", "Right")
				topDownRow = self.getRow("Top", "Down")
				rightLeftRow = self.getRow("Right", "Left")
				self.setRow("Bottom", "Up", rightLeftRow)
				self.setRow("Right", "Left", topDownRow)
				self.setRow("Top", "Down", leftRightRow)
				self.setRow("Left", "Right", bottomUpRow)
			if moveType == "B":
				frontFace = self.cube["Back"]
				self.setFace("Back", [list(ls) for ls in  zip(*frontFace[::-1])])
				topUpRow = self.getRow("Top", "Up")
				leftLeftRow = self.getRow("Left", "Left")
				topDownRow = self.getRow("Top", "Down")
				rightRightRow = self.getRow("Right", "Right")
				self.setRow("Top", "Up", rightRightRow)
				self.setRow("Right", "Right", topDownRow)
				self.setRow("Top", "Down", leftLeftRow)
				self.setRow("Left", "Left", topUpRow)
			if moveType == "L":
				frontFace = self.cube["Left"]
				self.setFace("Left", [list(ls) for ls in  zip(*frontFace[::-1])])
				leftUpRow = self.getRow("Left", "Up")
				leftFrontRow = self.getRow("Left", "Front")
				rightBackRow = self.getRow("Right", "Back")
				leftDownRow = self.getRow("Left", "Down")
				self.setRow("Right", "Back", leftDownRow)
				self.setRow("Left", "Up", rightBackRow)
				self.setRow("Left", "Down", leftFrontRow)
				self.setRow("Left", "Front", leftUpRow)
			if moveType == "R":
				frontFace = self.cube["Right"]
				self.setFace("Right", [list(ls) for ls in zip(*frontFace[::-1])])
				rightTopRow = self.getRow("Right", "Up")
				leftBackRow = self.getRow("Left", "Back")
				rightDownRow = self.getRow("Right", "Down")
				rightFrontRow = self.getRow("Right", "Front")
				self.setRow("Right", "Up", rightFrontRow)
				self.setRow("Right", "Front", rightDownRow)
				self.setRow("Right", "Down", leftBackRow)
				self.setRow("Left", "Back", rightTopRow)
			if moveType == "U":
				frontFace = self.cube["Up"]
				self.setFace("Up", [list(ls) for ls in zip(*frontFace[::-1])])
				topRightRow = self.getRow("Top", "Right")
				topFrontRow = self.getRow("Top", "Front")
				topLeftRow = self.getRow("Top", "Left")
				topBackRow = self.getRow("Top", "Back")
				self.setRow("Top", "Right", topBackRow)
				self.setRow("Top", "Back", topLeftRow)
				self.setRow("Top", "Left", topFrontRow)
				self.setRow("Top", "Front", topRightRow)
			if moveType == "D":
				frontFace = self.cube["Left"]
				self.setFace("Left", [list(ls) for ls in zip(*frontFace[::-1])])
				bottomFrontRow = self.getRow("Bottom", "Front")
				bottomRightRow = self.getRow("Bottom", "Right")
				bottomBackRow = self.getRow("Bottom", "Back")
				bottomLeftRow = self.getRow("Bottom", "Left")
				self.setRow("Bottom", "Front", bottomLeftRow)
				self.setRow("Bottom", "Left", bottomBackRow)
				self.setRow("Bottom", "Back", bottomRightRow)
				self.setRow("Bottom", "Right", bottomFrontRow)


if __name__ == "__main__":
	cube = Rubix()
	cube.move("B1")

